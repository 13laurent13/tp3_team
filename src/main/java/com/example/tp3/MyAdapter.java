package com.example.tp3;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.recyclerview_content, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Pair<String, String> paire = null;

        Context context = holder.itemView.getContext();
        SportDbHelper dbHelper = new SportDbHelper(context);
        Cursor result = dbHelper.fetchAllTeams();
        //SimpleCursorAdapter adapter = new SimpleCursorAdapter(context, R.layout.recyclerview_content, result, new String[]{SportDbHelper.COLUMN_TEAM_NAME, SportDbHelper.COLUMN_LEAGUE_NAME}, new int[]{R.id.name, R.id.league}, 0);

        result.moveToPosition(position);
        String name = result.getString(result.getColumnIndex(dbHelper.COLUMN_TEAM_NAME));
        String league = result.getString(result.getColumnIndex(dbHelper.COLUMN_LEAGUE_NAME));
        paire = Pair.create(name, league);
        /*
        String[] nom = AnimalList.getNameArray();
        Animal Name = AnimalList.getAnimal(nom[position]);
        String image = Name.getImgFile();
        try{
            int addr = R.drawable.class.getField(image).getInt(null);

            paire = Pair.create(nom[position], addr);
        }
        catch(Exception e){e.getMessage();}*/

        holder.display(paire);

    }

    @Override
    public int getItemCount(Context context) {
        SportDbHelper dbHelper = new SportDbHelper(context);
        return(dbHelper.fetchAllTeams().getCount());
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView name;
        private TextView league;

        public MyViewHolder(final View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.name);
            league = (TextView) itemView.findViewById(R.id.league);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(itemView.getContext(), TeamActivity.class);
                    String message = (String) name.getText();
                    intent.putExtra("MESSAGE", message);
                    view.getContext().startActivity(intent);
                }
            });
        }

        public void display(Pair<String, String> pair) {
            name.setText(pair.first);
            //name.setText(nom);
            league.setText(pair.second);
            //icon.setImageResource(add);
        }
    }

}